

text = "World"          # string        Python infered a string value type
print(text)             # World
print(type(text))       # class 'str'

number = 5              # integer       Python infered an integer value type
print(number)           # 5
print(type(number))     # class 'int'

decimal = 7.0           # float         Python infered an float value type
print(decimal)          #  7.0 
print(type(decimal))    # class 'float'

bolean = True           # boolean       Python infered an boolean value type
print(bolean)           # True
print(type(bolean))     # class 'bool'

# bool type is use to verify condition

