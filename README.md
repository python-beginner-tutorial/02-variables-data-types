# 02 Variables Data Types



## Name

Python Variable Types

## Description

A **variable** is container that hold a value that is use by the program. **Variables** are store in the computer memory and can be modified. In computer science this kind of variable are mutable variable

Python by default has infered mutable variable. In language like **Rust** you have to declare the type and the mutability of a variabe

Python is a dinamic language so you need to initialize variable at decaration

The main types of  

   - text          # string
   - numbers       # int, float
   - boolean       # bool

Example of python 

```python

    text = "World"  # string        Python infered a string value type
    type(text)      # class 'str'

    number = 5      # integer       Python infered an integer value type
    type(number)    # class 'int'

    decimal = 7.0   # float         Python infered an float value type
    type(decimal)   # class 'float'

    bolean = True   # boolean       Python infered an boolean value type
    type(bolean)    # class 'bool'

    # bool type is use to verify condition

```

## Usage

- Clone repository
```bash
git clone https://gitlab.com/python-beginner-tutorial/02-variables-data-types.git
cd 02-variables-data-types
```
- Run the file with python
```bash
python variables-type.py
```

